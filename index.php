<?php
// créer une variable globale : ici une constante:
// sert partout pour définir des chemis à partir de la racine
define('ROOT_PATH', __DIR__);

// Inclusion des classes et librairies utilisées
require 'classes/HtmlDocument.class.php' ;
require 'libs/mobile.lib.php' ; // Pour is_mobile()

//récupère le GET du nom de page de l'URL
//$page= $_GET['page'];

// Valeur par défaut pour la page si non renseignée
$page = isset($_GET['page']) ? $_GET['page'] : 'index' ;

//  interdire l'inclusion de fichiers situés à
//l'extérieur de l'arborescence de notre framework (Path Traversal attack)
if (strpbrk($page, '.<>!?:')){
    $page = 'erreur';
}
// Appel au constructeur : il lit le corps de la page et le mémorise dans la variable
//interne $mainContent
$doc = new HtmlDocument($page) ;

// Application du template désiré : on lit le fichier de template (ce dernier
//utilisera la fonction getMainContent() pour inclure le corps de la page à l'endroit
//souhaité) et on stoque le résultat dans la variable interne $bodyContent (= ce qu'il
//y a entre les balises <body> du document HTML)
$doc->applyTemplate( is_mobile() ? 'mobile' : 'defaut' ) ;


// On envoie au navigateur le code HTML complet de la page. C'est à ce stade que l'on
//génère la balise <html> mais aussi <head> pour inclure les styles CSS entre autre.
$doc->render();


?>
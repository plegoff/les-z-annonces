<?php
    include('classes/DbPetitesAnnonces.class.php');
    //Ajouter la prise en charge d'un paramètre $_GET['categorie']
    //à la page pour n'afficher que les annonces relatives à la catégorie
    //passée en paramètre
    $tableauAnnonces=array();
    $bdd = \DB\DbPetitesAnnonces::getInstance();

    try {
        /*
        $bdd = new PDO('mysql:host=localhost;dbname=annonces;charset=utf8mb4','root', '');
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        */
        $stmt = $bdd->query('SELECT * FROM annonces');
        

        // Met les données de la base mysql dans un tableau ($tableauAnnonces)
        while ($donnees = $stmt->fetch()) { 
            $tableauAnnonces[]=$donnees;
        };
        $stmt->closeCursor();
    }
    catch ( PDOException $e ) { die("Échec lors de la connexion : ".$e->getMessage()) ;
    }

    // ne pas oublier de mettre les htmlspecialchars() pour éviter les injections javascript
    // exemple: il encode < en &lt
    // mettre aussi dans les <a href="">
    echo '<table border="1px">';
    echo '<thead>';
        echo '<tr>';
            echo '<td>Date</td>';
            echo '<td>Titre</td>';
            echo '<td>Prix</td>';
            echo '<td>Contenu Annonce</td>';
        echo '</tr>';
    echo '</thead>';
        echo '<tbody>';
            // boucle prenant les données du tableau $tableau annonces et les insère
            //dans un tableau html
            foreach( $tableauAnnonces as $annonce ){
            echo '<tr>';
                echo '<td>'.htmlspecialchars($annonce['date']).'</td>';
                echo '<td>'.htmlspecialchars($annonce['titre']).'</td>';
                echo '<td>'.htmlspecialchars($annonce['prix']).' euros</td>';
                echo '<td>'.htmlspecialchars($annonce['contenu']).'</td>';
                $href='./index.php?page=annonces/annonce-modifier&idAnn='.$annonce["id_annonce"];
                echo '<td><a href="'.htmlspecialchars($href).'">Modifier Annonce</a></td>';
            echo '</tr>';
            }
        echo '</tbody>';
    echo '</table>';

?>
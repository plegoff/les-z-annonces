<?php
    include('classes/DbPetitesAnnonces.class.php');
   /**
    * Présente une annonce avec tous ses attributs
    *  sous forme de tableau à 2 entrées
    * la page reçoit l'id d'une annonce en GET (idAnn=)
    */
    $id_annonce = $_GET['idAnn'];
    //$id_annonce = "11633 OR 1=1";
    $tableauAnnonce=array();

    // instance de DbPetitesAnnonces : renvoie $instance
    $bdd = \DB\DbPetitesAnnonces::getInstance();
    
    //$bdd=$object->getInstance();
    var_dump($bdd);

    // Chargement de l'annonce sous forme de tableau dans $tableauAnnonce
    try {
        /*
        $bdd = new PDO('mysql:host=localhost;dbname=annonces;charset=utf8mb4','root', '');
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        */
        $stmt = $bdd->query('SELECT * FROM annonces WHERE id_annonce ='.$_GET['idAnn']);

        // Met les données de la table annonces dans un tableau ($tableauAnnonce)
        while ($donnees = $stmt->fetch()) { 
            $tableauAnnonce[]=$donnees;
        };
        $stmt->closeCursor();
    }   
    catch ( PDOException $e ) { die("Échec lors de la connexion : ".$e->getMessage()) ;
    }
    foreach( $tableauAnnonce as $annonce ){
    $idCat= $annonce['id_categorie'];
    }
    
    try {
        //$bdd = new PDO('mysql:host=localhost;dbname=annonces;charset=utf8mb4','root', '');
        //$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $bdd->query('SELECT libelle FROM categories WHERE id_categorie ='.$idCat);

        // Met le libellé de la catégorie actuelle dans $tabLib
        $lib = $stmt->fetch(PDO::FETCH_OBJ);
        
        $stmt->closeCursor();
    }
    catch ( PDOException $e ) { die("Échec lors de la connexion : ".$e->getMessage()) ;
    }

    $libelle= $lib->libelle;

    // remplit un tableau ($tableauCat) par le contenu de la table categories de la bdd
    try {
        //$bdd = new PDO('mysql:host=localhost;dbname=annonces;charset=utf8mb4','root', '');
        //$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $bdd->query('SELECT * FROM categories');

        // Met les données de la table categories dans un tableau ($tableauCat)
        while ($donnees = $stmt->fetch()) { 
            $tableauCat[]=$donnees;
        };
        $stmt->closeCursor();
    }
    catch ( PDOException $e ) { die("Échec lors de la connexion : ".$e->getMessage()) ;
    }

    // ne pas oublier de mettre les htmlspecialchars() pour éviter les injections javascript
    // exemple: il encode < en &lt
    // mettre aussi dans les <a href="">
    echo '<h1>MODIFIEZ VOTRE ANNONCE</h1><br>';

    /**
     * formulaire qui envoit les variables en POST vers annoncemodifier.action.main.php
     * 
     * noms des variables:
     * titre
     * date
     * textannonce
     * prix
     * idcat
     */
    echo '<form method="post" action="./index.php?page=annonces/annoncemodifier-action">';
    echo '<table border="1px">';

        echo '<tbody>';
            echo '<input type="hidden" name="idAnn" value="'.htmlspecialchars($id_annonce).'">';
            // boucle prenant les données du tableau $tableau annonces et les insère
            //dans un tableau html
            foreach( $tableauAnnonce as $annonce ){
                echo '<tr><th>Titre</th><td><input type="text" name="titre" value="'.htmlspecialchars($annonce['titre']).'"></td></tr>';
                echo '<tr><th>Date</th><td><input type="date" name="date" value="'.htmlspecialchars($annonce['date']).'" required></td></tr>';
                echo '<tr><th>Contenu</th><td><textarea id="story" name="textannonce"
                rows="10" cols="33">'.htmlspecialchars($annonce['contenu']).'</textarea></td></tr>';
                echo '<tr><th>Prix (euros)</th><td><input type="text" name="prix" value="'.htmlspecialchars($annonce['prix']).'"></td></tr>';
                echo '<tr><th>Catégorie</th><td>'.htmlspecialchars($libelle).'  ';

                echo '<select name="libcat" id="cat-select">';
                    echo '<option value="'.$libelle.'">--modifier catégorie--</option>';
                        // boucle prenant les données du tableau $tableauCat et les insère
                        //dans un select
                            foreach( $tableauCat as $categ ){
                                
                                echo '<option value="'.htmlspecialchars($categ['libelle']).'">'.htmlspecialchars($categ['libelle']).'</option>';
                }
                echo '</select>';
                echo '</td></tr>';
                echo '<tr><th>Id utilisateur</th><td><input type="text" name="id_utilisateur" value="'.htmlspecialchars($annonce['id_utilisateur']).'"></td></tr>';
    
            }
        echo '</tbody>';
    echo '</table>';
    echo '<input type="submit" value="Valider" />';
    echo '</form>';
?>
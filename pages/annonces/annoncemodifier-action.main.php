<?php
    include('classes/DbPetitesAnnonces.class.php');
    include('libs/http.lib.php');

    // au lieu de "die", on pourrait rediriger vers la même page, avec une alerte sur le champ non rempli
    if ($_POST['date']==''){
        die("date non renseignée ");
    }
    if ($_POST['idAnn']==''){
        die("id annonce non renseignée ");
    }
    if ($_POST['titre']==''){
        die("date non renseignée ");
    }
    if ($_POST['id_utilisateur']==''){
        die("id utilisateur non renseigné ");
    }
    if ($_POST['textannonce']==''){
        die("contenu annonce non renseignée ");
    }
    if($_POST['idAnn']==''){
        die("contenu annonce non renseignée ");
    }

    

    //récupère l'id_categorie en base à partir du libellé de la catégorie ($_POST['libcat'])
    try {
        // instance de DbPetitesAnnonces : renvoie $instance
        // il faut indiquer le namespace avec \DB\
        $bdd = \DB\DbPetitesAnnonces::getInstance();
        /*
        $bdd = new PDO('mysql:host=localhost;dbname=annonces;charset=utf8mb4','root', '');
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        */
        $stmt = $bdd->query('SELECT id_categorie FROM categories WHERE libelle ="'.$_POST['libcat'].'";');

        // Met l'id_categorie' de la catégorie actuelle dans $cat
        $cat = $stmt->fetch(PDO::FETCH_OBJ);
        
        $stmt->closeCursor();
    }
    catch ( PDOException $e ) { die("Échec lors de la connexion : ".$e->getMessage()) ;
    }

    $id_categorie= $cat->id_categorie;
    if ($id_categorie==''){
        die("id categorie non renseigné ");
    }

    echo 'id annonce: '.$_POST['idAnn'].'<br><br>';
    echo 'titre: '.$_POST['titre'].'<br><br>';
    echo 'texte annonce: '.$_POST['textannonce'].'<br><br>';
    echo 'prix: '.$_POST['prix'].'<br><br>';
    echo 'id_categorie: '.$id_categorie.'<br><br>';
    echo 'date: '.$_POST['date'].'<br><br>';
    echo 'id_utilisateur: '.$_POST['id_utilisateur'].'<br><br>';
    echo 'libelle categ: '.$_POST['libcat'].'<br><br>';
    echo 'id_annonce: '.$_POST['idAnn'].'<br><br>';
    

    $bdd = new PDO('mysql:host=localhost;dbname=annonces;charset=utf8mb4','root', '');
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //requête preparee
    // elle permmet d'éviter des injections sql type 11633 OR 1=1
    $req = $bdd->prepare('UPDATE annonces SET 
        id_annonce = :nvid_annonce, 
        titre = :nvtitre, 
        contenu = :nvcontenu, 
        prix = :nvprix, 
        id_categorie = :nvid_categorie, 
        date = :nvdate, 
        id_utilisateur = :nvid_utilisateur 
        WHERE id_annonce = '.$_POST['idAnn'].'');

    $req->execute(array(
        'nvid_annonce' => $_POST['idAnn'],
        'nvtitre' => $_POST['titre'],
        'nvcontenu' => $_POST['textannonce'],
        'nvprix' => $_POST['prix'],
        'nvid_categorie' => $id_categorie,
        'nvdate' => $_POST['date'],
        'nvid_utilisateur' => $_POST['id_utilisateur']
    ));
    
    //header('Location: index.php');
    header('Location: index.php?page=annonces/annonce-modifier-validee');

    $url="index.php?page=annonces/annonce-modifier-validee";
    
    //Effectue une redirection (303 par defaut)
    http_redirect(htmlspecialchars($url), $code=303, $allow_external_domain=false);

    
?>
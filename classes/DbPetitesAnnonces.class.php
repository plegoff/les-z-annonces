<?php
namespace DB;
/**
 * Classe DbPetitesAnnonces étendant de PDO
 * créee pour y stocker les infos de connexion + gestion des erreurs
 * (les appels éventuels à setAttribute n'apparaitront que dans le
 * constructeur de cette classe)
 */
class DbPetitesAnnonces extends \PDO {
    
    private static $instance;

    public function __construct() {
        
        $this->DB_HOST = "localhost";
        $this->DB_NAME = "annonces";
        $this->DB_USER = "root";
        $this->DB_PASS = "";
        $this->CHARSET = "utf8mb4";

        // ("data source name") correspond au 1er paramètre à entrer dans PDO()
        $dsn = "mysql:host=".$this->DB_HOST.";dbname=".$this->DB_NAME.";charset=".$this->CHARSET;
        parent::__construct($dsn, $this->DB_USER, $this->DB_PASS);
        try{
        // la connexion PDO
        $this->instance = new \PDO($dsn,$this->DB_USER,$this->DB_PASS);

        //$instance = new PDO('mysql:host=localhost;dbname=annonces;charset=utf8mb4','root', '');
        $this->instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        //return $instance;
        }
        catch(PDOException $e){
            echo 'echec de la connexion'.$e->getMessage();
        }
    }
    /**
     * fonction qui renvoie une instance statique de DbPetitesAnnonces
     * (elle ouvre une connexion)
     */
    public static function getInstance(){
        if (self::$instance === null){
            self::$instance = new DbPetitesAnnonces();
        }
        return self::$instance;
    }

    /**
     * fonction qui ferme la connexion
     */
    public static function close(){
        self::$instance=null;

    }
}

?>
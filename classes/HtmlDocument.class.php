<?php
/**
 * Moteur de template simple.
 *
 * Les pages doivent être stockées dans le répertoire pages/ et porter l'extension '.main.php'.
 * Une page peut retourner l'exception NotConnectedException si elle requière la connexion de l'utilisateur pour être affichée.
 * Les templates doivent être stockées dans le répertoire templates/. 
 * Ce sont des répertoires qui portent le nom du template et doivent contenir le fichier 'template.php'
 * (ainsi que les éventuelles ressources liées au template: images, etc...)
 *
 * Utilisation basique :
 *   $doc = new HtmlDocument('mapage') ;
 *   $doc->applyTemplate('defaut') ;
 *   $doc->render() ;
 */
class HtmlDocument {

    static private $currentInstance;

    static public function getCurrentInstance(){

        return self::$currentInstance;

    }

    protected $mainFilePath;
    protected $templateName;
    protected $headers;
    protected $bodyContent;
    protected $mainContent;
    
    /**
     * Constructeur qui prend en param un nom de fichier sans le ".main.php"
     * correspond à la partie <section> de la page
     */
    public function __construct($mainFile){

        // on peut utiliser self::$currentInstance à la place de HtmlDocument
        if (HtmlDocument::$currentInstance !== null) throw new Exception("non autorisé");
        HtmlDocument::$currentInstance = $this;

        $this->headers= array(
            1=> '<meta charset="utf-8" />',
            2=> '<link rel="stylesheet" href="templates/defaut/styles/style.css" />',
            3=> '<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->',
            4=> '<title>Les Z Annonces</title>'
        );
        
        $this->mainFilePath= __DIR__.'/../pages/'.$mainFile.'.main.php';
        $this->templateName='';
        $this->mainContent='';
        $this->bodyContent='';

        $this->parseMain();
        
        
    }

    /**
     * Fonction qui met un chemin sous forme de variable $mainFilePath dans $mainContent
     */
    protected function parseMain(){
        // fonction de bufferisation
        //On appelle la fonction ob_start() qui "mémorise" toute la sortie HTML qui suit,
        // puis, à la fin, on récupère le contenu généré avec ob_get_clean() et on met le tout dans $mainContent
        try{
        ob_start();
        include $this->mainFilePath;
        $this->mainContent=ob_get_clean();
        }
        catch(NotConnectedException $e){
            echo 'Une exception a été lancée. Message d\'erreur : ', $e->getMessage();
        }
        
    }


    // Application du template désiré : on lit le fichier de template (ce dernier
    //utilisera la fonction getMainContent() pour inclure le corps de la page à l'endroit
    //souhaité) et on stoque le résultat dans la variable interne $bodyContent (= ce qu'il
    //y a entre les balises <body> du document HTML)


    /**
     * Fonction qui met le code html du template dans la variable bodyContent
     */
    protected function parseTemplate(){
        //On appelle la fonction ob_start() qui "mémorise" toute la sortie HTML qui suit,
        // puis, à la fin, on récupère le contenu généré avec ob_get_clean() et on met le tout dans $bodyContent
        ob_start();
        include __DIR__.'/../templates/'.$this->templateName.'/template.php';
        $this->bodyContent=ob_get_clean();
    }

    /**
     * Fonction qui renseigne le nom du template à appliquer
     */
    public function applyTemplate($templateName){
        $this->templateName=$templateName;
        $this->parseTemplate();
    }

    /**
     * Fonction qui écrit le code dynamiquement
     */
    public function render(){
        //affiche le template
        echo "<!DOCTYPE html>\n" ;
        echo "<html>" ;
        echo "<head>" ;
        echo $this->getHead();
        echo "</head>\n" ;

        echo $this->bodyContent;
        echo "</head>\n" ;
    }

    /**
     * Fonction qui renvoie le contenu du fichier .main.php courant
     */
    public function getMainContent(){
        return $this->mainContent;
    }

    /**
     * Fonction qui retourne la partie head du html en parcourant
     * le tableau headers
     */
    public function getHead(){
        $head = '' ;
        foreach($this->headers as $value){
            $head .= $value.'<br/>';  // opérateur combiné équivalent à $head = $head.$value.'<br/>';
        }
        return $head ;
    }

    /**
     * Fonction qui ajoute une ligne au tableau du head (appelé headers)
     * @param: $html (string)
     * @param: $position (entier)
     */
    public function addHeader($html, $position){
        $this->headers[$position]=$html;
       
    }

    /**
     * Effectue une redirection (303 par defaut)
     * @param String $url  URL de redirection
     * @param int    $code HTTP response code (303 par defaut)
     * @param bool   $allow_external_domain Pour authoriser des redirections vers des domaines autres qu'EasyZic
     */
    function http_redirect($url, $code=303, $allow_external_domain=false) {
        if ( $url{0}==='/' && @$url{1}!=='/' ) $url = '//'.$_SERVER['HTTP_HOST'].$url ;
        http_response_code($code) ;
        header("Location: ".$url) ;
        echo "<a href='".htmlentities($url)."'>Cliquez sur ce lien si la page ne s'affiche pas !</a>" ;
        exit() ;
    }

}